import os
import logging
import sys
from Config import config
from flask import Flask, jsonify, session, g, request
import pythoncom
import wmi
from blueprints import folder_management, win_management

def create_app(config):
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(config)

    @app.route('/')
    def homepage():
        session['os'] = sys.platform
        return 'Python System Controller v-0.0', 200

    @app.route('/taskList')
    def task_list():
        if session['os'] != 'win32':
            logger.info('Running on %s Windows endpoints not available'%session['os'])
        return jsonify(return_task_defined())

    if sys.platform == 'win32':
        app.register_blueprint(win_management.bp)

    @app.before_request
    def init_env():
        if 'system/' in request.url:
            pythoncom.CoInitialize()
            g.c = wmi.WMI()


    app.register_blueprint(folder_management.bp)
    app.add_url_rule('/', endpoint='index')

    return app


def return_task_defined():
    os_tasks = {'OS TASKS': {'endpoint': '/system/<taskName>'}}
    folder_tasks = {'Folder Task':  {'endpoint': '/folder/<taskName>'}}
    if session['os'] != 'win32':
        return folder_tasks
    return {**os_tasks, **folder_tasks}

if __name__ == '__main__':
    flask_config = config.Config
    app = create_app(flask_config)
    log_format = format="LogTime:%(asctime)s, 'Logger': %(name)s, 'LogLevel:%(levelname)s, LogDetail:[%(name)s.%(funcName)s:%(lineno)d], LogMessage:%(message)s"
    logging.basicConfig(level=logging.DEBUG, format=log_format)
    PORT = os.environ.get('FLASK_PORT', 5535)
    logger = logging.getLogger(__name__)
    logger.info('Starting System management')
    app.run(debug=True, host='0.0.0.0', port=PORT)
