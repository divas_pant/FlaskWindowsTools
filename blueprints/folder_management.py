import os
import logging
import re
import time
import hashlib
import pprint
import hashlib
from flask import Blueprint, jsonify, request, abort

logger = logging.getLogger(__name__)

bp = Blueprint('folder_management', __name__, url_prefix='/folder')

@bp.route('/')
def display_task_list():
    return jsonify({'Folder Task':  {'endpoint': '/folder/<taskName>',
                                    'taskName':{'getSize': 'Return sizes for files in folder',
                                                 'getTime': 'Return time modified for all files in folder',
                                                 'renameFolders': 'Renames folder in dir to sequence Folder name provided',
                                                 'renameFiles': 'Renames files in dir to sequence Files name provided',
                                                 'md5Checksum': 'Generates checksum for files in the given directoy',
                                                 'verifyShaChecksum ': 'Compares SHA256 checksum of file and validates with input',
                                                 'findDuplicates': 'Find duplicate files in same folder or two folders',
                                                 'verifyFile': 'Compares checksum of file with given value and returns Boolean',
                                                 'findAndReplace': 'Finds files with given search strings and renames them'}}})

@bp.route('/<task>', methods=['GET', 'POST'])
def exec_task(task):
    data = request.form.to_dict()
    val = return_task_func(task)(data)
    return jsonify(val), 200

def return_task_func(task):
    if task not in task_dict():
        abort(404)
    return task_dict().get(task, None)

def task_dict():
    task_list = {'getSize': get_file_sizes,
                'getTime': get_file_modified,
                'renameFolders': sequence_rename_folders,
                'renameFiles': sequence_rename_files,
                'md5Checksum': gen_checksum_for_folder,
                'verifyShaChecksum': eval_sha256,
                'findDuplicates': find_duplicate_files,
                'verifyFile': verify_file_checksum,
                'findAndReplace': find_and_rename}
    return task_list



def get_file_sizes(data):
    dir = data.get('dir', None)
    if not dir or not os.path.isdir(dir):
        return 'Error in request: Missing/Incorrect parameter [dir]', 1400
    file_data = {}
    for file in os.listdir(dir):
        try:
            if os.path.isdir('/'.join([dir,file])):
                continue
            size = int(os.path.getsize('/'.join([dir,file])))/1e6
            file_data.update({str(file): str(size)+' MB'})
        except Exception as e:
            logger.error('Error in reading files %s'%e)
    return file_data


def get_file_modified(data):
    dir = data.get('dir', None)
    if not dir or not os.path.isdir(dir):
        return 'Error in request: Missing/Incorrect parameter [dir]', 1400
    file_data = {}
    for file in os.listdir(dir):
        try:
            if os.path.isdir('/'.join([dir, file])):
                continue
            file_data.update({file: time.localtime(os.path.getmtime('/'.join([dir, file])))})
        except os.error:
            loger.error('Error in getting data %s'%e)

    return file_data

def sequence_rename_folders(data):
    '''Renames all folder in the destination,
    if no input name is supplied, Folder is used '''
    source_dir = data.get('dir', None)
    if not source_dir or not os.path.isdir(source_dir):
        return 'Error in request: Missing/Incorrect parameter [source_dir]', 1400
    name = data.get('name', 'Folder')
    file_data = {'originalName': 'newName'}
    i = 0
    for file in os.listdir(source_dir):
        try:
            if not os.path.isdir('/'.join([source_dir, file])):
                continue
            new_name = name+'_'+str(i)
            file_data.update({file: new_name})
            os.rename('/'.join([source_dir, file]), '/'.join([source_dir, new_name]))
            i += 1
        except os.error as e:
            logger.error('Error in Renaming folders %s'%e)
            continue
    return file_data


def sequence_rename_files(data):
    '''Renames all files in the destination '''
    source_dir = data.get('dir', None)
    if not source_dir or not os.path.isdir(source_dir):
        return 'Error in request: Missing/Incorrect data [dir]', 1400
    name = data.get('name', None)
    if not name or name == '':
        name = 'File'
    file_data = {}
    i = 0
    for file in os.listdir(source_dir):
        try:
            if os.path.isdir('/'.join([source_dir, file])):
                continue
            new_name = name+'_'+str(i)+'.'+file.split('.')[-1]
            file_data.update({file: new_name})
            os.rename('/'.join([source_dir, file]), '/'.join([source_dir, new_name]))
            i += 1
        except os.error as e:
            logger.error('Error in Renaming files %s'%e)
            continue
    return file_data


def gen_checksum_for_folder(data):
    '''Generates md5 checksum for files in directory'''
    dir = data.get('dir', None)
    if not dir or not os.path.isdir(dir):
        return 'Error in request: Missing/Incorrect parameter [dir]', 1400
    file_data = {}
    for file in os.listdir(dir):
        try:
            if os.path.isdir('/'.join([dir, file])):
                continue
            with open('/'.join([dir, file]),'rb')as f:
                hash = hashlib.md5(f.read()).hexdigest()
            file_data.update({file: hash})
        except os.error as e:
            logger.error('Error in generating md5 checksum %s'%e)
            continue
    return file_data


def find_duplicate_files(data_in):
    ''' Returns list of duplicate files and checksum value'''
    comparison_data = {}
    data = {'dir': data_in.get('dir1')}
    chksum_output = gen_checksum_for_folder(data)
    if 1400 in chksum_output:
        return 'Error in Request: Missing/Incorrect parameter [dir1]', 1400
    comparison_data.update({'folder1' : chksum_output})
    data = {'dir': data_in.get('dir2', None)}
    if data['dir'] is not None:
        chksum_output = gen_checksum_for_folder(data)
        if 1400 in chksum_output:
            return 'Error in Request: Missing/Incorrect parameter [dir2]'
        comparison_data.update({'folder2' : chksum_output})
    file_data = compare_checksum_duplicate(comparison_data)
    return file_data


def compare_checksum_duplicate(data):
    similar_files = []
    folder1 = data['folder1']
    if len(data.keys()) == 2:
        folder2 = data['folder2']
        for key, val in folder1.items():
            # if val in
            similar_files.append((val, k) for k,v in folder2.items() if v == val)
    else:
        hashes = folder1.values()
        for key, val in folder1.items():
             for k,v in folder1.items():
                if v == val and k != key:
                    similar_files.append((key, k))
    return {'duplicate_files': similar_files}

def verify_file_checksum(data):
    file = data.get('file', None)
    if file is None or os.path.isdir(file):
        return 'Error in request: Missing/Incorrect Data [file]', 1400
    hash_in = data.get('md5', None)
    if hash_in is None or len(hash_in) != 32:
        return 'Error in request: Missing/Incorrect data hash_in'
    with open(file,'rb')as f:
        hash = hashlib.md5(f.read()).hexdigest()
    if hash == hash_in:
        return True
    else:
        return False

def find_and_rename(data):
    find = data.get('find', None)
    new_name = data.get('rename', None)
    start = data.get('start', None)
    end = data.get('ends', None)
    contains = data.get('contains', None)
    dir = data.get('dir', None)
    if not find and not start and not ends and not contains:
        return 'Error in request: Missing/Incorrect Data: search parameters', 1400
    if not new_name:
        return 'Error in request: Missing/Incorrect Data [rename]'
    if not os.path.isdir(dir):
        return 'Error in request: Missing/Incorrect Data [dir]'
    i = 0
    for file in os.listdir(dir):
        try:
            if os.path.isdir('/'.join([dir, file])):
                continue
            if find:
                match = re.match(r'\b %s.[a-z, 1-9]'%find, file)
                if match:
                    os.rename('/'.join([dir, file]), '/'.join([dir, new_name]))
                    i += 1
                    continue
            if start and start != '':
                match = re.match(r'%s(?=\w|\s|\.|\b)'%start, file)
                if match:
                    os.rename('/'.join([dir, file]), '/'.join([dir, new_name]))
                i += 1
                continue
            elif end:
                match = re.match(r'(.%s(?!=\w+|\s) %end', file)
                if match:
                    os.rename('/'.join([dir, file]), '/'.join([dir, new_name]))
                    i += 1
                    continue
            elif contains:
                match = re.match(r'(?=\w|\s|\b)%s(?=\w|\s|\b)'%contains, file)
                if match:
                    os.rename('/'.join([dir, file]), '/'.join([dir, new_name]))
                    i += 1
                    continue

        except os.error as e:
            logger.error('Some error occured in renaming files %s'%e)
            continue

    return 'Found & Updated %s files'%str(i), 1200


def find_and_replace(data):
    find = data.get('find', None)
    if not find:
        return 'Error in request: Missing/Incorrect Data [find]'
    dir = data.get('dir', None)
    if not dir:
        return 'Error in request: Missing/Incorrect Data [dir]'
    replace = data.get('replace', None)
    if not replace:
        return 'Error in request: Missing/Incorrect data [replace]'
    i = 0
    for file in os.listdir(dir):
        try:
            if os.path.isdir('/'.join([dir, file])):
                continue
            new_name = file
            re.sub(find, replace, new_name)
            if new_name != file:
                os.rename('/'.join([dir, file]), '/'.join([dir, new_name]))
                i += 1
        except os.error as e:
            logger.error('Some error occured in renaming files %s'%e)
    return 'Updated %s records'%str(i), 1200



def eval_sha256(data):
    file = data.get('file', None)
    if not file or os.path.isdir(file):
        return 'Error in Request: Missing/Incorrect parameter [file]', 1400
    hash_in = data.get('sha', None)
    if not hash_in or len(hash_in) != 64:
        return 'Error in Request: Missing/Incorrect parameter [sha]', 1400
    with open(file, "rb") as f:
        bytes = f.read()
        sha = hashlib.sha256(bytes).hexdigest()
    if sha == hash_in:
        return True, 1200
    return False, 1400
