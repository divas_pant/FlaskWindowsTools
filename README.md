# Flask Windows Tools

Flask Windows tools consists set of methods to simplify some of common tasks required for sys admin duties and general tweaking. 
These methods are served over Flask WSGI server running locally to simplify director paths and accesses

## Installation

The program can be installed in virtual environment running oon Pipenv, and the .lock file can be used to replicate process with consistency.
To install from pip file, first install pipenv from pypi

```bash
pip install pipenv
```
followed by 
```bash
pipenv install --deploy
```
to installed from pipfile.lock, or 
```bash
pipenv install
```
to install from pipfle directly. 
Pipfile is not locked on version as .lock file handles that

## Usage
To run the project, enter python shell for pipenv by doing
```bash
pipenv shell
```
followed by 
```bash 
app.py
```
or execute directly from folder by
```bash 
pipenv run python3 app.py
```


## Contributing
Pull requests are welcome. For major changes or new suggestions, please open an issue first to discuss what you would like to change or suggest.


## License
[MIT](https://choosealicense.com/licenses/mit/)
