import os
import logging
import  subprocess
from pathlib import Path
from flask import Blueprint, jsonify, g

logger = logging.getLogger(__name__+'_winManagement')


bp = Blueprint('win_management', __name__, url_prefix='/system')

@bp.route('/')
def return_task_list():
    os_tasks = {'OS TASKS': {'endpoint': '/system/<taskName>',
                                'taskName':{'info': 'To get OS information',
                                            'services': 'To list running services',
                                            'drives': 'List Available drive on the system',
                                            'newProcess': 'See new Processes starting',
                                            'batteryReport': 'To see the battery report'}}}
    return jsonify(os_tasks)


@bp.route('/<task>')
def exec_task(task):
    val = return_task_func(task)(g.c) if return_task_func(task) else None

    if val is None:
        return 'Task Not found', 400
    return jsonify(val), 200

def return_task_func(task):
    return task_dict().get(task, None)

def task_dict():
    task_list = {'info': get_os_info,
                'services': see_services,
                'drives': see_drives,
                'newProcess': see_new_process,
                'batteryReport': gen_battery_report,
                'installedPrograms': get_installed_programs}
    return task_list


def get_os_info(c):
    os_info = {'OS': []}
    for os in c.Win32_OperatingSystem():
        os_info['OS'].append(os.Caption)
    return os_info

def see_new_process(c):
    process_watcher = c.Win32_Process.watch_for("creation")
    while True:
      new_process = process_watcher()
      print( new_process.Caption)
    return

def see_modified_process(c):
    process_watcher = c.Win32_Process.watch_for("modification")
    while True:
      new_process = process_watcher()
      print( new_process.Caption)

def see_services(c):
    out = {}
    for services in c.Win32_Service():
        out.update({services.DisplayName :services.ProcessId})
    return out

def see_drives(c):
    disk_drives = {}
    for disk in c.Win32_LogicalDisk():
        disk_drives.update({disk.DeviceID: int(disk.Size)/(1024*1024)})
    disk_drives.update({'Total Size': sum(disk_drives.values())})
    return disk_drives

def get_devices(c):
    for device in c.Win32_DeviceSettings():
        print(device.Setting.Caption)

def gen_battery_report(c):
    res = subprocess.call('powercfg /batteryreport')
    if res !=0:
        return 'Error in generating report'
    return 'Report Successfully Generated'

def get_groups_users(task):
    for group in c.Win32_Group():
        print(group.Caption)
        for user in group.associators(wmi_result_class="Win32_UserAccount"):
            print(" [+]", user.Caption)


def get_installed_programs(c):
    out = {}
    for item in c.Win32_Product():
        if item.Name is None:
            continue
        out.update({str(item.Name): str(item.Version)} )
    return out
    

if __name__ == '__main__':
    c = wmi.WMI()
    logger = logging.getLogger(__name__)
    for val in c.classes:
        if 'Win' in val:
            print(val)

